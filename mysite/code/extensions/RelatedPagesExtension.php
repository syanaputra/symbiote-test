<?php

class RelatedPagesExtension extends DataExtension
{
    /**
     * @var null
     */
    var $cacheRelatedPages = NULL;  // Store related pages in memory

    /**
     * @var array
     */
    private static $many_many = array(
        'RelatedPages' => 'Page',
    );

    /**
     * @var array
     */
    private static $belongs_many_many = array(
        'OfRelatedPages' => 'Page.RelatedPages',
    );

    /**
     * Update CMS fields to include Related Pages
     *
     * @param FieldList $fields
     */
    public function updateCMSFields(FieldList $fields)
    {
        $page = $this->owner;

        if ($page->ID > 0) {
            // Get pages aside from current. Because we are not relating to self
            $pages = Page::get()->exclude('ID', $page->ID);

            $relatedPagesField = ListboxField::create('RelatedPages', 'Related Pages', $pages->map('ID', 'Title')->toArray())->setDescription('Select pages that relates to this page.')->setMultiple(true);
            $fields->addFieldsToTab('Root.Main', $relatedPagesField, 'Metadata');

            $ofRelatedPagesField = Gridfield::create('OfRelatedPages', 'Being Related By', $page->OfRelatedPages(), $config = GridFieldConfig_RecordViewer::create());
            $fields->addFieldsToTab('Root.BeingRelatedBy', $ofRelatedPagesField);
        }
    }

    /**
     * Get all related pages of this page
     *
     * @return null|ArrayList
     */
    public function AllRelatedPages() {
        if(!$this->cacheRelatedPages) {
            $page = $this->owner;

            // Get related pages
            $relatedPages = $page->RelatedPages();

            // Get the pages who relate to this page
            $ofRelatedPages = $page->OfRelatedPages();

            // Store everything in ArrayList
            $data = ArrayList::create();
            $data->merge($relatedPages);
            $data->merge($ofRelatedPages);

            // Remove Duplicates just in case
            $data->removeDuplicates('ID');

            // Cache it to get faster performance :D
            $this->cacheRelatedPages = $data;
        }

        return $this->cacheRelatedPages;
    }
}
