## Assumptions

The following explains the assumption that are taken when building this test:

 * `$AllRelatedPages` is used to get all related pages and pages that are being related with
 * **Related Pages** and **Being Related By** are only visible when page has been saved (when we have ID)
 * Temporary cache memory is used on `$AllRelatedPages` to speed up the process to not always load from database everytime the function is called. 
