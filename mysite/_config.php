<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	'type' => 'MySQLDatabase',
	'server' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'symbiote',
	'path' => ''
);

// Set the site locale
i18n::set_locale('en_AU');
ini_set('date.timezone', 'Australia/Melbourne');
date_default_timezone_set('Australia/Melbourne');

// Remove this when going LIVE
Security::setDefaultAdmin('admin','admin');
